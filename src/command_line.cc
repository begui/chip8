#include "command_line.hh"
#include <getopt.h>
#include <iostream>
#include "version.hh"

const std::string& CommandLine::rom ( ) const { return rom_; }
bool CommandLine::fullscreen ( ) const { return fullscreen_; }
bool CommandLine::debug ( ) const { return debug_; }
void CommandLine::resetOptIndex() {
  // Global value
  ::optind = 1;
}

void CommandLine::menuHelp ( ) const {
  std::cout << "Chip8 " << CHIP8_VERSION_STRING << std::endl;
  std::cout << "Usage example:" << std::endl;
  std::cout << "(-r|--rom) string [(-h|--help)] [(-f|--fullscreen)] [(-d|--debug)]" << std::endl;
  std::cout << "Options:" << std::endl;
  std::cout << "-h or --help: Displays this information." << std::endl;
  std::cout << "-r or --rom string: Path to Rom file. Required." << std::endl;
//  std::cout << "-f or --fullscreen: Run in fullscreen." << std::endl;
  std::cout << "-d or --debug: Start in debug mode." << std::endl;
}

bool CommandLine::captureArg ( int argc, char* argv[] ) {
  // GetOpt option definition
  char* optRom = 0;
  static const char* const shortOptions = "hr:fd";
  static constexpr struct option longOptions[] = {{"help", 0, NULL, 'h'},
                                           {"rom", 1, NULL, 'r'},
                                           {"fullscreen", 0, NULL, 'f'},
                                           {"debug", 0, NULL, 'd'},
                                           {NULL, 0, NULL, 0}};

  while ( true ) {
    auto next_option = getopt_long ( argc, argv, shortOptions, longOptions, NULL );
    if ( next_option == -1 ) break;  // No more options. Break loop.

    switch ( next_option ) {
      case 'r':  // -r or --rom
        optRom = optarg;
        rom_ = optRom;
        break;
      case 'f':  // -f or --fullscreen
        fullscreen_ = true;
        break;
      case 'd':  // -d or --debug
        debug_ = true;
        break;
      case -1:  // No more options
        break;
      case 'h':  // -h or --help
      case '?':  // Invalid option
      default:
        menuHelp ( );
        return false;
    }
  }

  // Check for mandatory arguments
  if ( !optRom ) {
    printf ( "Mandatory arguments not specified\n" );
    menuHelp ( );
    return false;
  }
  return true;
}
