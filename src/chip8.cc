#include "chip8.hh"

#include <algorithm>
#include <chrono>
#include <cstdio>
#include <fstream>
#include <iomanip>
#include <random>
#include <sstream>

using namespace std::placeholders;

const unsigned char Font[] = {
    0xF0, 0x90, 0x90, 0x90, 0xF0,  // 0
    0x20, 0x60, 0x20, 0x20, 0x70,  // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0,  // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0,  // 3
    0x90, 0x90, 0xF0, 0x10, 0x10,  // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0,  // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0,  // 6
    0xF0, 0x10, 0x20, 0x40, 0x40,  // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0,  // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0,  // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90,  // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0,  // B
    0xF0, 0x80, 0x80, 0x80, 0xF0,  // C
    0xE0, 0x90, 0x90, 0x90, 0xE0,  // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0,  // E
    0xF0, 0x80, 0xF0, 0x80, 0x80   // F
};
constexpr auto FontSize{ 16 * 5 };

const static auto seed = std::chrono::system_clock::now ( ).time_since_epoch ( ).count ( );
static std::mt19937 gen ( seed );
auto random_number = [] ( ) {
  std::uniform_int_distribution<> dis ( 0, 255 );
  return dis ( gen );
};

auto instruction_string = [] ( auto programCounter, auto opcode, auto name ) {
  std::ostringstream buffer;
  buffer << "0x" << std::setfill ( '0' ) << std::setw ( 4 ) << std::hex << programCounter << "-0x" << opcode
         << " :: " << name;
  return buffer.str ( );
};

Chip8::Chip8 ( ) {
  opMap = {
      { 0x0000, std::bind ( &Chip8::op_0000, this, _1 ) },  //
      { 0x1000, std::bind ( &Chip8::op_1000, this, _1 ) },  //
      { 0x2000, std::bind ( &Chip8::op_2000, this, _1 ) },  //
      { 0x3000, std::bind ( &Chip8::op_3000, this, _1 ) },  //
      { 0x4000, std::bind ( &Chip8::op_4000, this, _1 ) },  //
      { 0x5000, std::bind ( &Chip8::op_5000, this, _1 ) },  //
      { 0x6000, std::bind ( &Chip8::op_6000, this, _1 ) },  //
      { 0x7000, std::bind ( &Chip8::op_7000, this, _1 ) },  //
      { 0x8000, std::bind ( &Chip8::op_8000, this, _1 ) },  //
      { 0x9000, std::bind ( &Chip8::op_9000, this, _1 ) },  //
      { 0xA000, std::bind ( &Chip8::op_A000, this, _1 ) },  //
      { 0xB000, std::bind ( &Chip8::op_B000, this, _1 ) },  //
      { 0xC000, std::bind ( &Chip8::op_C000, this, _1 ) },  //
      { 0xD000, std::bind ( &Chip8::op_D000, this, _1 ) },  //
      { 0xE000, std::bind ( &Chip8::op_E000, this, _1 ) },  //
      { 0xF000, std::bind ( &Chip8::op_F000, this, _1 ) }   //
  };
}

/*
 *
 * 0nnn - SYS addr
 * Jump to a machine code routine at nnn. This instruction is only used on the old computers on
 * which Chip-8 was originally implemented. It is ignored by modern interpreters.
 * 00E0 - CLS
 * Clear the display.
 * 00EE - RET
 * Return from a subroutine. The interpreter sets the program counter to the address at
 * the top of the stack, then subtracts 1 from the stack pointer.
 *
 */
void Chip8::op_0000 ( const std::uint16_t opcd ) noexcept {
  switch ( opcd & 0x00FF ) {
    case 0x00E0:
      instructionStr_ = instruction_string ( pcPrev_, opcd, "00E0 - CLS - Clear the display" );
      display_.fill ( 0 );
      break;
    case 0x00EE:
      instructionStr_ = instruction_string ( pcPrev_, opcd, "00EE - RET - Return from a subroutine" );
      pc_ = stack_[ sp_-- ];
      break;
    default:
      instructionStr_ = instruction_string ( pcPrev_, opcd, "Unable to Process Instruction. Should be ignored? " );
      break;
  }
}
/*
 * 1nnn - JP addr
 * Jump to location nnn. The interpreter sets the program counter to nnn.
 */
void Chip8::op_1000 ( const std::uint16_t opcd ) noexcept {
  instructionStr_ = instruction_string ( pcPrev_, opcd, "1nnn - JP addr - Jump to location nnn" );
  std::uint16_t valueNNN = opcd & 0x0FFF;
  pc_ = valueNNN;
}
/*
 * 2nnn - CALL addr
 * Call subroutine at nnn. The interpreter increments the stack pointer,
 * then puts the current PC on the top of the stack. The PC is then set to nnn.
 *
 */
void Chip8::op_2000 ( const std::uint16_t opcd ) noexcept {
  instructionStr_ = instruction_string ( pcPrev_, opcd, "2nnn - CALL  addr - Call subroutine at nnn" );
  std::uint16_t valueNNN = opcd & 0x0FFF;
  // We never use the  0th element
  stack_[ ++sp_ ] = pc_;
  pc_ = valueNNN;
}
/*
 * 3xkk - SE Vx, byte
 * Skip next instruction if Vx = kk.The interpreter compares register
 * Vx to kk, and if they are equal, increments the program counter by 2.
 *
 */
void Chip8::op_3000 ( const std::uint16_t opcd ) noexcept {
  instructionStr_ = instruction_string ( pcPrev_, opcd, "3xkk - SE Vx, byte - Skip next instruction if Vx = kk" );
  std::uint8_t indexX = static_cast< std::uint8_t > ( ( opcd & 0x0F00 ) >> 8 );
  std::uint8_t value = static_cast< std::uint8_t > ( opcd & 0x00FF );
  if ( register_[ indexX ] == value ) {
    pc_ += 2;
  }
}
/*
 * 4xkk - SNE Vx, byte
 * Skip next instruction if Vx != kk. The interpreter compares register
 * Vx to kk, and if they are not equal, increments the program counter by 2.
 */
void Chip8::op_4000 ( const std::uint16_t opcd ) noexcept {
  instructionStr_ = instruction_string ( pcPrev_, opcd, "4xkk - SNE Vx, byte - Skip next instruction if Vx != kk" );
  std::uint8_t indexX = static_cast< std::uint8_t > ( ( opcd & 0x0F00 ) >> 8 );
  std::uint8_t value = static_cast< std::uint8_t > ( opcd & 0x00FF );
  if ( register_[ indexX ] != value ) {
    pc_ += 2;
  }
}
/*
 * 5xy0 - SE Vx, Vy
 * Skip next instruction if Vx = Vy. The interpreter compares register Vx to
 * register Vy, and if they are equal, increments the program counter by 2.
 */
void Chip8::op_5000 ( const std::uint16_t opcd ) noexcept {
  instructionStr_ = instruction_string ( pcPrev_, opcd, "5xy0 - SE Vx, Vy - Skip next instruction if Vx = Vy" );
  std::uint8_t indexX = static_cast< std::uint8_t > ( ( opcd & 0x0F00 ) >> 8 );
  std::uint8_t indexY = static_cast< std::uint8_t > ( ( opcd & 0x00F0 ) >> 4 );
  if ( register_[ indexX ] == register_[ indexY ] ) {
    pc_ += 2;
  }
}
/*
 * 6xkk - LD Vx, byte
 * Set Vx = kk. The interpreter puts the value kk into register Vx.
 */
void Chip8::op_6000 ( const std::uint16_t opcd ) noexcept {
  instructionStr_ = instruction_string ( pcPrev_, opcd, "6xkk - LD Vx, byte - Set Vx = kk" );
  std::uint8_t indexX = static_cast< std::uint8_t > ( ( opcd & 0x0F00 ) >> 8 );
  std::uint8_t value = static_cast< std::uint8_t > ( opcd & 0x00FF );
  register_[ indexX ] = value;
}
/*
 * 7xkk - ADD Vx, byte
 * Set Vx = Vx + kk.Adds the value kk to the value of register Vx,
 * then stores the result in Vx.
 * */
void Chip8::op_7000 ( const std::uint16_t opcd ) noexcept {
  instructionStr_ = instruction_string ( pcPrev_, opcd, "7xkk - ADD Vx, byte - Set Vx = Vx + kk" );
  std::uint8_t indexX = static_cast< std::uint8_t > ( ( opcd & 0x0F00 ) >> 8 );
  std::uint8_t value = static_cast< std::uint8_t > ( opcd & 0x00FF );
  register_[ indexX ] += value;
}
/*
 * 8xy0 - LD Vx, Vy
 * Set Vx = Vy. Stores the value of register Vy in register Vx.
 * 8xy1 - OR Vx, Vy
 * Set Vx = Vx OR Vy.Performs a bitwise OR on the values of Vx and Vy, then stores
 * the result in Vx. A bitwise OR compares the corrseponding bits from two
 * values, and if either bit is 1, then the same bit in the result is also 1. Otherwise, it is 0.
 * 8xy2 - AND Vx, Vy
 * Set Vx = Vx AND Vy.
 * Performs a bitwise AND on the values of Vx and Vy, then stores the result in Vx. A bitwise AND
 * compares the corrseponding bits from two values, and if both bits are 1, then the same
 * bit in the result is also 1. Otherwise, it is 0.
 * 8xy3 - XOR Vx, Vy
 * Set Vx = Vx XOR Vy.Performs a bitwise exclusive OR on the values of Vx and Vy, then
 * stores the result in Vx. An exclusive OR compares the corrseponding bits from two values, and if the
 * bits are not both the same, then the corresponding bit in the result is set to 1. Otherwise, it is 0.
 * 8xy4 - ADD Vx, Vy
 * Set Vx = Vx + Vy, set VF = carry. The values of Vx and Vy are added together. If the result is
 * greater than 8 bits (i.e., > 255,) VF is set to 1, otherwise 0. Only the lowest 8 bits of the result are kept, and
 * stored in Vx.
 * 8xy5 - SUB Vx, Vy
 * Set Vx = Vx - Vy, set VF = NOT borrow. If Vx > Vy, then VF is set to 1, otherwise 0. Then Vy is subtracted from Vx,
 * and the results stored in Vx.
 * 8xy6 - SHR Vx {, Vy}
 * Set Vx = Vx SHR 1. If the least-significant bit of Vx is 1, then VF is set to 1, otherwise 0. Then Vx is divided by
 * 2.
 * 8xy7 - SUBN Vx, Vy
 * Set Vx = Vy - Vx, set VF = NOT borrow. If Vy > Vx, then VF is set to 1, otherwise 0. Then Vx is subtracted from Vy,
 * and the results stored in Vx.
 * 8xyE - SHL Vx {, Vy}
 * Set Vx = Vx SHL 1.If the most-significant bit of Vx is 1, then VF is set to 1, otherwise to 0. Then Vx is multiplied
 * by 2.
 */
void Chip8::op_8000 ( const std::uint16_t opcd ) noexcept {
  std::uint8_t indexX = static_cast< std::uint8_t > ( ( opcd & 0x0F00 ) >> 8 );
  std::uint8_t indexY = static_cast< std::uint8_t > ( ( opcd & 0x00F0 ) >> 4 );

  switch ( opcd & 0x000F ) {
    case 0x0000:
      instructionStr_ = instruction_string ( pcPrev_, opcd, "8xy0 - LD Vx, Vy :: Set Vx = Vy" );
      register_[ indexX ] = register_[ indexY ];
      break;
    case 0x0001:
      instructionStr_ = instruction_string ( pcPrev_, opcd, "8xy1 - OR Vx, Vy :: Set Vx = Vx OR Vy" );
      register_[ indexX ] |= register_[ indexY ];
      break;
    case 0x0002:
      instructionStr_ = instruction_string ( pcPrev_, opcd, "8xy2 - AND Vx, Vy :: Set Vx = Vx AND Vy" );
      register_[ indexX ] &= register_[ indexY ];
      break;
    case 0x0003:
      instructionStr_ = instruction_string ( pcPrev_, opcd, "8xy3 - XOR Vx, Vy :: Set Vx = Vx XOR Vy" );
      register_[ indexX ] ^= register_[ indexY ];
      break;
    case 0x0004: {
      instructionStr_ = instruction_string ( pcPrev_, opcd, "8xy4 - ADD Vx, Vy :: Set Vx = Vx + Vy" );
      std::uint16_t result = register_[ indexX ] + register_[ indexY ];
      register_[ 0xF ] = ( result >> 8 );  // get Carry
      register_[ indexX ] = result & 0xFF;
    } break;
    case 0x0005: {
      instructionStr_ = instruction_string ( pcPrev_, opcd, "8xy5 - SUB Vx, Vy :: Set Vx = Vx - Vy" );
      std::uint16_t result = register_[ indexX ] - register_[ indexY ];
      register_[ 0xF ] = !( result >> 8 );  // get Carry
      register_[ indexX ] = result & 0XFF;
    } break;
    case 0x0006:
      instructionStr_ = instruction_string ( pcPrev_, opcd, "8xy6 - SHR Vx {, Vy} :: Set Vx = Vx SHR 1" );
      register_[ 0xF ] = register_[ indexX ] & 0x1;
      // register_[ indexX ] >>= 1;
      register_[ indexX ] = register_[ indexY ] >> 1;
      break;
    case 0x0007: {
      instructionStr_ = instruction_string ( pcPrev_, opcd, "8xy7 - SUBN Vx, Vy :: Set Vx = Vy - Vx" );
      std::uint16_t result = register_[ indexY ] - register_[ indexX ];
      register_[ 0xF ] = !( result >> 8 );  // get Carry
      register_[ indexX ] = result & 0XFF;
    } break;
    case 0x000E:
      instructionStr_ = instruction_string ( pcPrev_, opcd, "8xyE - SHL Vx {, Vy} :: Set Vx = Vy SHL 1" );
      register_[ 0xF ] = register_[ indexY ] >> 7;
      register_[ indexX ] = register_[ indexY ] << 1;
      break;
    default:
      instructionStr_ = instruction_string ( pcPrev_, opcd, "Unable to process" );
      break;
  }
}

/*
 * 9xy0 - SNE Vx, Vy
 * Skip next instruction if Vx != Vy. The values of Vx and Vy are compared, and if they are
 * not equal, the program counter is increased by 2.
 */
void Chip8::op_9000 ( const std::uint16_t opcd ) noexcept {
  instructionStr_ = instruction_string ( pcPrev_, opcd, "9xy0 - SNE Vx, Vy :: Skips instruction if Vx != Vy" );
  std::uint8_t indexX = static_cast< std::uint8_t > ( ( opcd & 0x0F00 ) >> 8 );
  std::uint8_t indexY = static_cast< std::uint8_t > ( ( opcd & 0x00F0 ) >> 4 );
  if ( register_[ indexX ] != register_[ indexY ] ) {
    pc_ += 2;
  }
}
/*
 * Annn - LD I, addr
 * Set I = nnn. The value of register I is set to nnn.
 */
void Chip8::op_A000 ( const std::uint16_t opcd ) noexcept {
  instructionStr_ = instruction_string ( pcPrev_, opcd, "Annn - LD I, addr :: Set I = nnn" );
  std::uint16_t valueNNN = opcd & 0x0FFF;
  indexRegister_ = valueNNN;
}
/**
 * Bnnn - JP V0, addr
 * Jump to location nnn + V0.The program counter is set to nnn plus the value of V0.
 */
void Chip8::op_B000 ( const std::uint16_t opcd ) noexcept {
  instructionStr_ = instruction_string ( pcPrev_, opcd, "Bnnn - JP V0, addr :: Jump to location nnn" );
  std::uint16_t valueNNN = opcd & 0x0FFF;
  pc_ = valueNNN + register_[ 0 ];
}
/*
 * Cxkk - RND Vx, byte
 * Set Vx = random byte AND kk.
 * The interpreter generates a random number from 0 to 255, which is then ANDed with the value kk.
 * The results are stored in Vx. See instruction 8xy2 for more information on AND.
 */
void Chip8::op_C000 ( const std::uint16_t opcd ) noexcept {
  instructionStr_ = instruction_string ( pcPrev_, opcd, "Cxkk - RND Vx, byte:: Set Vx = random byte and kk" );
  std::uint8_t indexX = static_cast< std::uint8_t > ( ( opcd & 0x0F00 ) >> 8 );
  std::uint8_t value = static_cast< std::uint8_t > ( ( opcd & 0x00FF ) );
  register_[ indexX ] = random_number ( ) & value;
}

/*
 * Dxyn - DRW Vx, Vy, nibble
 * Display n-byte sprite starting at memory location I at (Vx, Vy), set VF = collision.
 * The interpreter reads n bytes from memory, starting at the address stored in I. These
 * bytes are then displayed as sprites on screen at coordinates (Vx, Vy). Sprites are
 * XORed onto the existing screen. If this causes any pixels to be erased, VF is set
 * to 1, otherwise it is set to 0. If the sprite is positioned so part of it is outside
 * the coordinates of the display, it wraps around to the opposite side of the screen.
 * See instruction 8xy3 for more information on XOR.
 */
void Chip8::op_D000 ( const std::uint16_t opcd ) noexcept {
  instructionStr_ = instruction_string ( pcPrev_, opcd, "Dxyn - DRW Vx, Vy, nibble :: Draws" );
  const std::uint8_t indexX = static_cast< std::uint8_t > ( ( opcd & 0x0F00 ) >> 8 );
  const std::uint8_t indexY = static_cast< std::uint8_t > ( ( opcd & 0x00F0 ) >> 4 );
  const std::uint8_t x = register_[ indexX ];
  const std::uint8_t y = register_[ indexY ];
  const std::uint8_t height = static_cast< std::uint8_t > ( opcd & 0x000F );

  std::uint8_t pixel;
  register_[ 0xF ] = 0;
  for ( uint8_t yline = 0; yline < height; yline++ ) {
    pixel = memory_[ indexRegister_ + yline ];
    for ( uint8_t bit = 0; bit < 8; bit++ ) {
      if ( ( pixel & ( 0x80 >> bit ) ) != 0 ) {
        auto index = ( x + bit ) % Chip8::WidthSize + ( ( ( y + yline ) % Chip8::HeightSize ) * Chip8::WidthSize );
        if ( display_[ index ] == 1 ) {
          register_[ 0xF ] = 1;
        }
        display_[ index ] ^= 1;
      }
    }
  }
  draw_ = true;
}
/*
 * Ex9E - SKP Vx
 * Skip next instruction if key with the value of Vx is pressed.Checks the keyboard, and if
 * the key corresponding to the value of Vx is currently in the down position, PC is increased by 2.
 * ExA1 - SKNP Vx
 * Skip next instruction if key with the value of Vx is not pressed.
 * Checks the keyboard, and if the key corresponding to the value of Vx is currently in the
 * up position, PC is increased by 2.
 */
void Chip8::op_E000 ( const std::uint16_t opcd ) noexcept {
  std::uint8_t indexX = static_cast< std::uint8_t > ( ( opcd & 0x0F00 ) >> 8 );

  switch ( opcd & 0x00FF ) {
    case 0x009E:
      instructionStr_ = instruction_string ( pcPrev_, opcd, "Ex9E - SKP Vx :: Skip next instruction" );
      if ( key[ register_[ indexX ] ] != 0 ) {
        pc_ += 2;
      }
      break;
    case 0x00A1:
      instructionStr_ =
          instruction_string ( pcPrev_, opcd, "ExA1 - SKNP Vx :: Skip next instruction if value of Vx is not pressed" );
      if ( key[ register_[ indexX ] ] == 0 ) {
        pc_ += 2;
      }
      break;
    default:
      instructionStr_ = instruction_string ( pcPrev_, opcd, "Unable to process" );
      break;
  }
}
/*
 * Fx07 - LD Vx, DT
 * Set Vx = delay timer value. The value of DT is placed into Vx.
 * Fx0A - LD Vx, K
 * Wait for a key press, store the value of the key in Vx. All execution stops until a key
 * is pressed, then the value of that key is stored in Vx.
 * Fx15 - LD DT, Vx
 * Set delay timer = Vx. DT is set equal to the value of Vx.
 * Fx18 - LD ST, Vx
 * Set sound timer = Vx. ST is set equal to the value of Vx.
 * Fx1E - ADD I, Vx
 * Set I = I + Vx. The values of I and Vx are added, and the results are stored in I.
 * Fx29 - LD F, Vx
 * Set I = location of sprite for digit Vx.The value of I is set to the location for the
 * hexadecimal sprite corresponding to the value of Vx. 0-F in hex are a 4x5 font.
 * Fx33 - LD B, Vx
 *. The interpreter
 * takes the decimal value of Vx, and places the hundreds digit in memory at location
 * in I, the tens digit at location I+1, and the ones digit at location I+2.
 * Fx55 - LD [I], Vx
 * Store registers V0 through Vx in memory starting at location I. The interpreter
 * copies the values of registers V0 through Vx into memory, starting at the address in I.
 * Fx65 - LD Vx, [I]
 * Read registers V0 through Vx from memory starting at location I. The interpreter
 * reads values from memory starting at location I into registers V0 through Vx.
 */
void Chip8::op_F000 ( const std::uint16_t opcd ) noexcept {
  bool incrementPC{ true };
  std::uint8_t indexX = static_cast< std::uint8_t > ( ( opcd & 0x0F00 ) >> 8 );
  switch ( opcd & 0x00FF ) {
    case 0x0007:
      instructionStr_ = instruction_string ( pcPrev_, opcd, "Fx07 - LD Vx, DT :: Set Vx = delay timer value" );
      register_[ indexX ] = delayTimer_;
      break;
    case 0x000A:
      instructionStr_ = instruction_string (
          pcPrev_, opcd, "Fx0A - LD Vx, K :: Wait for a key press, store the value of the key in Vx" );
      incrementPC = false;
      for ( size_t i{ 0 }; i < key.size ( ); ++i ) {
        if ( key[ i ] != 0 ) {
          incrementPC = true;
          register_[ indexX ] = i;
        }
      }
      break;
    case 0x0015:
      instructionStr_ = instruction_string ( pcPrev_, opcd, "Fx15 - DL ST, Vx :: Sets delay timer = Vx" );
      delayTimer_ = register_[ indexX ];
      break;
    case 0x0018:
      instructionStr_ = instruction_string ( pcPrev_, opcd, "Fx18 - Set Sound timer = Vx" );
      soundTimer_ = register_[ indexX ];
      break;
    case 0x001E:
      instructionStr_ = instruction_string ( pcPrev_, opcd, "Fx1E - ADD I, Vx :: Set I = I + Vx" );
      indexRegister_ += register_[ indexX ];
      break;
    case 0x0029:
      instructionStr_ =
          instruction_string ( pcPrev_, opcd, "Fx29 - LD F, Vx :: Set I = location of sprite for digit Vx" );
      // TODO: this might be wrong
      indexRegister_ = ( register_[ indexX ] & 0xF ) * 0x5;
      break;
    case 0x0033:
      instructionStr_ = instruction_string (
          pcPrev_, opcd, "Fx33 - LD B, Vx :: Store BCD representation of Vx in memory locations I, I+1, and I+2 " );
      memory_[ indexRegister_ ] = ( register_[ indexX ] / 100 ) % 10;
      memory_[ indexRegister_ + 1 ] = ( register_[ indexX ] / 10 ) % 10;
      memory_[ indexRegister_ + 2 ] = ( register_[ indexX ] % 100 ) % 10;
      break;
    case 0x0055:
      instructionStr_ = instruction_string (
          pcPrev_, opcd, "Fx55 - LD [I], Vx :: Store registers V0 through Vx in memory starting at location I" );
      for ( auto i{ 0 }; i <= indexX; ++i ) {
        memory_[ indexRegister_ + i ] = register_[ i ];
      }
      indexRegister_ += indexX + 1;
      break;
    case 0x0065:
      instructionStr_ = instruction_string (
          pcPrev_, opcd, "Fx65 - LD Vx, [I] :: Read registers V0 through Vx from memory starting at location I" );
      for ( auto i{ 0 }; i <= indexX; ++i ) {
        register_[ i ] = memory_[ indexRegister_ + i ];
      }
      // TODO: fix?
      indexRegister_ += indexX + 1;
      break;
    default:
      instructionStr_ = instruction_string ( pcPrev_, opcd, "Unable to process" );
      incrementPC = false;
      break;
  }
  if ( !incrementPC ) {
    pc_ -= 2;
  }
}

void Chip8::loadRom ( std::string_view romFile ) {
  reset ( );
  // Load program into Ram starting at location 512
  std::ifstream fin ( romFile.data ( ), std::ios::in | std::ios::binary );
  if ( fin.is_open ( ) ) {
    std::istreambuf_iterator< char > iterbegin ( fin );
    std::istreambuf_iterator< char > iterend{ };
    std::copy ( iterbegin, iterend, memory_.begin ( ) + Chip8::ProgramCounterStart );
    fin.close ( );
  } else {
    throw std::runtime_error ( "Failed to open {" + std::string ( romFile ) + "}" );
  }

  opcode_ = opcodeCurrent ( );
}

void Chip8::reset ( ) {
  pc_ = Chip8::ProgramCounterStart;
  pcPrev_ = Chip8::ProgramCounterStart;
  opcode_ = 0;
  sp_ = 0;
  indexRegister_ = 0;
  memory_.fill ( 0 );
  register_.fill ( 0 );
  display_.fill ( 0 );
  key.fill ( 0 );
  stack_.fill ( 0 );
  soundTimer_ = 0;
  delayTimer_ = 0;

  // Load Font into Ram at location 0
  std::copy_n ( Font, FontSize, memory_.begin ( ) );
}

void Chip8::cycle ( ) {
  if ( !paused_ ) {
    // fetch the first opcode that is two bytes.
    // We take the byte from 'pc', shift 8 ( now two bytes ) and or it with the  other byte.
    // Example:
    //          memory_[pc] = 0xaa and memory_[pc+1] = 0xff. Shifting by 8
    //          converts to two bytes (0xaa00). Or-ing will give result 0xaaff

    if ( soundTimer_ > 0 ) {
      --soundTimer_;
    }
    if ( delayTimer_ > 0 ) {
      --delayTimer_;
    }

    opcode_ = opcodeCurrent ( );
    pcPrev_ = pc_;
    pc_ += 2;
    // check this first Bit to find its function
    auto search = opMap.find ( opcode_ & 0xF000 );
    if ( search != opMap.end ( ) ) {
      search->second ( opcode_ );
    } else {
      instructionStr_ = instruction_string ( pcPrev_, opcode_, "Not Implemented" );
    }

    /*
    printf ( "0x%04X\n", opcode_ );
    printf ( "Register dump:\n\t  0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F\nV[]\t= " );
    for ( auto i{ 0 }; i < Chip8::RegisterSize; i++ ) printf ( "%03X ", register_[ i ] );
    printf ( "\nS[]\t= " );
    for ( auto i{ 0 }; i < Chip8::StackSize; i++ ) printf ( "%03X ", stack_[ i ] );
    printf ( "\nSP\t= 0x%X\nI\t= 0x%X\nPC\t= 0x%X\nDT\t= 0x%X\nST\t= 0x%X\n", sp_, indexRegister_, pc_, delayTimer_,
             soundTimer_ );
    */
  }
}

void Chip8::pause ( bool pause ) noexcept { paused_ = pause; }
void Chip8::draw ( bool draw ) noexcept { draw_ = draw; }

void Chip8::stepCycle ( ) noexcept {
  if ( paused_ ) {
    paused_ = false;
    cycle ( );
    paused_ = true;
  }
}

const std::string &Chip8::instructionStr ( ) const { return instructionStr_; }
std::uint16_t Chip8::programCounter ( ) const { return pc_; }
std::uint16_t Chip8::opcode ( ) const { return opcode_; }

std::uint16_t Chip8::opcodeCurrent ( ) const {
  return memory_[ pc_ ] << 8 | memory_[ pc_ + 1 ];  //
}
std::uint16_t Chip8::stackPointer ( ) const { return sp_; }
std::uint16_t Chip8::indexRegister ( ) const { return indexRegister_; }
const std::array< std::uint8_t, Chip8::MemorySize > &Chip8::memory ( ) const { return memory_; }
const std::array< std::uint8_t, Chip8::RegisterSize > &Chip8::registers ( ) const { return register_; }
const std::array< std::uint8_t, Chip8::DisplaySize > &Chip8::display ( ) const { return display_; }
const std::array< std::uint16_t, Chip8::StackSize > &Chip8::stack ( ) const { return stack_; }
