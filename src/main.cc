#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "chip8.hh"
#include "command_line.hh"
#include "gd/gd_sdl2.hh"
#include "open_sans_font.hh"

constexpr auto WIDTH{ Chip8::WidthSize * 16 };
constexpr auto HEIGHT{ Chip8::HeightSize * 16 };
constexpr auto FONTSIZE{ 14 };

constexpr std::uint8_t keys[ 16 ]{ SDLK_1, SDLK_2, SDLK_3, SDLK_4,  //
                                   SDLK_q, SDLK_w, SDLK_e, SDLK_r,  //
                                   SDLK_a, SDLK_s, SDLK_d, SDLK_f,  //
                                   SDLK_z, SDLK_x, SDLK_c, SDLK_v };
struct Sdl {
  Sdl ( ) {
    sdl2::init ( );
    sdl2::ttf::init ( );
  }
  virtual ~Sdl ( ) {
    sdl2::quit ( );
    sdl2::ttf::quit ( );
  }

  sdl2::WindowUniqPtr window{ nullptr };
  sdl2::RendererUniqPtr renderer{ nullptr };
  sdl2::TextureUniqPtr drawTexture{ nullptr };
  sdl2::TTFUniqPtr ttf{ nullptr };
  const SDL_Rect drawTextureRect{ 0, 0, WIDTH, HEIGHT };  // TODO: fix this
};
//
// Function Prototypes
//
void debug_screen ( Sdl& sdl, const Chip8& chip8 ) noexcept;
//
// Main
//
int main ( int argc, char* argv[] ) {
  // Capture command line arguments
  CommandLine commandLine;
  if ( commandLine.captureArg ( argc, argv ) == false ) {
    return EXIT_SUCCESS;
  }
  try {
    Chip8 chip8;
    chip8.loadRom ( commandLine.rom ( ) );
    Sdl sdl2;
    sdl2.window = sdl2::create_window< sdl2::WindowUniqPtr > ( { WIDTH, HEIGHT } );
    const auto renderFlags =
        sdl2::internal::enum_class_or ( sdl2::RendererType::Accelerated, sdl2::RendererType::Vsync );
    sdl2.renderer = sdl2::create_renderer< sdl2::RendererUniqPtr > (  //
        sdl2.window.get ( ),                                          //
        sdl2::BlendmodeType::Blend,                                   //
        renderFlags );
    sdl2.drawTexture = sdl2::create_texture< sdl2::TextureUniqPtr > (  //
        sdl2.window.get ( ), sdl2.renderer.get ( ),                    //
        sdl2::TextureAccessType::Streaming,                            //
        Chip8::WidthSize, Chip8::HeightSize );
    sdl2.ttf = sdl2::ttf::load< sdl2::TTFUniqPtr > ( OpenSans_Regular_ttf, OpenSans_Regular_ttf_len, FONTSIZE );

    SDL_Event event;
    bool running{ true };
    bool debug = commandLine.debug ( );
    chip8.pause ( debug );
    while ( running ) {
      chip8.cycle ( );
      while ( SDL_PollEvent ( &event ) ) {
        switch ( event.type ) {
          case SDL_QUIT:
            running = false;
            break;
          case SDL_KEYDOWN: {
            if ( event.key.repeat == 0 ) {
              for ( int i = 0; i < 16; ++i ) {
                if ( event.key.keysym.sym == keys[ i ] ) {
                  chip8.key[ i ] = 1;
                }
              }
              switch ( event.key.keysym.sym ) {
                case SDLK_F1:
                  debug = !debug;
                  chip8.pause ( debug );
                  break;
                case SDLK_ESCAPE:
                  running = false;
                  break;
                default:
                  break;
              }
            }
            if ( event.key.keysym.sym == SDLK_RIGHT ) {
              chip8.stepCycle ( );
            }
          } break;
          case SDL_KEYUP: {
            for ( int i = 0; i < 16; ++i ) {
              if ( event.key.keysym.sym == keys[ i ] ) {
                chip8.key[ i ] = 0;
              }
            }
          } break;
        }
      }
      if ( chip8.shouldDraw ( ) || debug ) {
      sdl2::clear ( sdl2.renderer.get ( ) );
        if ( chip8.shouldDraw ( ) ) {
          chip8.draw ( false );
          if ( sdl2::render_to_pixel (
                   [ &chip8 ] ( std::uint32_t* pixels ) {
                     for ( size_t i = 0; i < chip8.display ( ).size ( ); ++i ) {
                       // multiplying by white if display value is set
                       pixels[ i ] = ( 0xFFFFFFFF * chip8.display ( )[ i ] );
                     }
                   },
                   sdl2.drawTexture.get ( ) ) ) {
          }
        }
        sdl2::render_texture ( sdl2.renderer.get ( ), sdl2.drawTexture.get ( ), nullptr, &sdl2.drawTextureRect );

        if ( debug ) {
          debug_screen ( sdl2, chip8 );
        }
        sdl2::swap ( sdl2.renderer.get ( ) );  // This needs to be called anytime we render a texture
      }
    }
  } catch ( const std::exception& e ) {
    std::cout << e.what ( ) << std::endl;
  }

  return EXIT_SUCCESS;
}

// TODO: TTF renders Slowly, refactor when time permits
void debug_screen ( Sdl& sdl2, const Chip8& chip8 ) noexcept {
  auto print = [ &sdl2 ] ( const char* text, int x, int y ) {
    const SDL_Color red{ 255, 0, 0, 255 };
    auto sur = sdl2::ttf::render< sdl2::SurfaceUniqPtr > ( sdl2.ttf.get ( ), text, red );
    auto tex = sdl2::surface_to_texture< sdl2::TextureUniqPtr > ( sdl2.renderer.get ( ), std::move ( sur ) );
    auto [ texW, texH ] = sdl2::get_texture_dimension ( tex.get ( ) );
    SDL_Rect rect{ x, y, texW, texH };  // TODO: fix this
    sdl2::render_texture ( sdl2.renderer.get ( ), tex.get ( ), nullptr, &rect );
  };

  sdl2::render_rect_fill ( sdl2.renderer.get ( ), SDL_Rect{ 0, 0, WIDTH, HEIGHT }, SDL_Color{ 0, 0, 0, 230 } );

  std::ostringstream buffer;
  int x = 0, y = 0;
  buffer.str ( "" );
  buffer << "PC:     " << std::setfill ( '0' ) << std::setw ( 4 ) << std::hex << chip8.programCounter ( );
  print ( buffer.str ( ).c_str ( ), x, y );

  buffer.str ( "" );
  buffer << "COpcode: " << std::setfill ( '0' ) << std::setw ( 4 ) << std::hex << chip8.opcodeCurrent ( );
  print ( buffer.str ( ).c_str ( ), x, y += FONTSIZE );

  buffer.str ( "" );
  buffer << "POpcode: " << std::setfill ( '0' ) << std::setw ( 4 ) << std::hex << chip8.opcode ( );
  print ( buffer.str ( ).c_str ( ), x, y += FONTSIZE );

  buffer.str ( "" );
  buffer << "Index:  " << std::hex << chip8.indexRegister ( );
  print ( buffer.str ( ).c_str ( ), x, y += FONTSIZE );

  buffer.str ( "" );
  buffer << "StackP: " << std::dec << chip8.stackPointer ( );
  print ( buffer.str ( ).c_str ( ), x, y += FONTSIZE );

  // Register Dump
  x += 110, y = 0;
  buffer.str ( "Registers:" );
  print ( buffer.str ( ).c_str ( ), x, y );
  for ( size_t i = 0; i < chip8.registers ( ).size ( ); ++i ) {
    buffer.str ( "" );
    buffer << "v[ " << std::hex << i << " ] " << std::hex << std::setw ( 2 )
           << static_cast< std::uint32_t > ( chip8.registers ( )[ i ] );
    print ( buffer.str ( ).c_str ( ), x, y += FONTSIZE );
  }

  // Stack
  x += 100, y = 0;
  buffer.str ( "Stack :" );
  print ( buffer.str ( ).c_str ( ), x, y );
  for ( size_t i = 0; i < chip8.stack ( ).size ( ); ++i ) {
    buffer.str ( "" );
    buffer << "s[ " << std::hex << i << " ] " << std::hex << std::setw ( 2 )
           << static_cast< std::uint32_t > ( chip8.stack ( )[ i ] );
    print ( buffer.str ( ).c_str ( ), x, y += FONTSIZE );
  }

  x += 100, y = 0;
  buffer.str ( "Keys :" );
  print ( buffer.str ( ).c_str ( ), x, y );
  for ( size_t i = 0; i < chip8.key.size ( ); ++i ) {
    buffer.str ( "" );
    buffer << "s[ " << std::hex << i << " ] " << std::hex << std::setw ( 2 )
           << static_cast< std::uint32_t > ( chip8.key[ i ] );
    print ( buffer.str ( ).c_str ( ), x, y += FONTSIZE );
  }

  x += 100, y = 0;
  buffer.str ( chip8.instructionStr ( ) );
  print ( buffer.str ( ).c_str ( ), x, y );
}
