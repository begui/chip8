#pragma once
#include <string>

class CommandLine {
 public:
  CommandLine ( ) = default;
  virtual ~CommandLine ( ) = default;

 public:
  const std::string& rom ( ) const;
  bool fullscreen ( ) const;
  bool debug ( ) const;

 public:
  void menuHelp ( ) const;
  bool captureArg ( int argc, char* argv[] );
  void resetOptIndex ( );

 private:
  std::string rom_;
  bool fullscreen_{ false };
  bool debug_{ false };
};
