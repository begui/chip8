#pragma once
#include <array>
#include <functional>
#include <string_view>
#include <unordered_map>

class Chip8 final {
 public:
  Chip8 ( );
  virtual ~Chip8 ( ) = default;

 public:
  static constexpr auto WidthSize{ 64 };
  static constexpr auto HeightSize{ 32 };
  static constexpr auto MemorySize{ 0x1000 };          // 4096
  static constexpr auto ProgramCounterStart{ 0x200 };  // 512
  static constexpr auto ProgramSize{ MemorySize - ProgramCounterStart };
  static constexpr auto DisplaySize{ WidthSize * HeightSize };
  static constexpr auto StackSize{ 16 };
  static constexpr auto RegisterSize{ 16 };
  static constexpr auto KeySize{ 16 };

 public:
  void loadRom ( std::string_view romFile );
  void reset ( );
  void cycle ( );
  void stepCycle ( ) noexcept;
  void pause ( bool pause ) noexcept;
  bool isPaused ( ) const noexcept { return paused_; }
  void draw ( bool draw ) noexcept;
  bool shouldDraw ( ) const noexcept { return draw_; }

 public:
  const std::string &instructionStr ( ) const;
  std::uint16_t programCounter ( ) const;
  std::uint16_t opcode ( ) const;
  std::uint16_t opcodeCurrent ( ) const;
  std::uint16_t stackPointer ( ) const;
  std::uint16_t indexRegister ( ) const;
  const std::array< std::uint8_t, MemorySize > &memory ( ) const;
  const std::array< std::uint8_t, RegisterSize > &registers ( ) const;
  const std::array< std::uint8_t, DisplaySize > &display ( ) const;
  const std::array< std::uint16_t, StackSize > &stack ( ) const;

 public:
  std::array< std::uint8_t, KeySize > key;  // Hex keyboard

 private:
  std::string instructionStr_{ " " };
  std::uint8_t soundTimer_{ 0 };
  std::uint8_t delayTimer_{ 0 };
  std::uint16_t pc_{ ProgramCounterStart };            // Program Counter
  std::uint16_t pcPrev_{ ProgramCounterStart };        // Program Counter
  std::uint16_t opcode_{ 0 };                          // 35 Opcodes that are two bytes
  std::uint16_t sp_{ 0 };                              // Stack pointer
  std::uint16_t indexRegister_{ 0 };                   // two byte memory address register 'i'
  std::array< std::uint8_t, MemorySize > memory_;      // Memory 4k
  std::array< std::uint8_t, RegisterSize > register_;  // 16 8-bit registers v0, v1, ... vF
  std::array< std::uint8_t, DisplaySize > display_;    // Display 64x32
  std::array< std::uint16_t, StackSize > stack_;       // Stack used to remember the current position

 private:
  bool paused_{ false };
  bool draw_{ false };

 private:
  std::unordered_map< std::uint16_t, std::function< void ( const std::uint16_t opcode ) > > opMap;
  void op_0000 ( const std::uint16_t opcode ) noexcept;
  void op_1000 ( const std::uint16_t opcode ) noexcept;
  void op_2000 ( const std::uint16_t opcode ) noexcept;
  void op_3000 ( const std::uint16_t opcode ) noexcept;
  void op_4000 ( const std::uint16_t opcode ) noexcept;
  void op_5000 ( const std::uint16_t opcode ) noexcept;
  void op_6000 ( const std::uint16_t opcode ) noexcept;
  void op_7000 ( const std::uint16_t opcode ) noexcept;
  void op_8000 ( const std::uint16_t opcode ) noexcept;
  void op_9000 ( const std::uint16_t opcode ) noexcept;
  void op_A000 ( const std::uint16_t opcode ) noexcept;
  void op_B000 ( const std::uint16_t opcode ) noexcept;
  void op_C000 ( const std::uint16_t opcode ) noexcept;
  void op_D000 ( const std::uint16_t opcode ) noexcept;
  void op_E000 ( const std::uint16_t opcode ) noexcept;
  void op_F000 ( const std::uint16_t opcode ) noexcept;
};
