# Chip 8



### Command Line Options

    Usage example:
    (-r|--rom) string [(-h|--help)] [(-f|--fullscreen)] [(-d|--debug)]
    Options:
    -h or --help: Displays this information.
    -r or --rom string: Path to Rom file. Required.
    -d or --debug: Start in debug mode.






### Resources

[Chip8-Wiki](https://en.wikipedia.org/wiki/CHIP-8)

[MattMik](http://mattmik.com/files/chip8/mastering/chip8.html)

[Cowgod Chip8](http://devernay.free.fr/hacks/chip8/C8TECH10.HTM)

[How to write a Chip8 Emu](http://www.multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter)

[BCD](https://en.wikipedia.org/wiki/Binary-coded_decimal)

[Rom Set](https://www.zophar.net/pdroms/chip8/chip-8-games-pack.html)

[Open Sans](https://www.fontsquirrel.com/fonts/open-sans)
