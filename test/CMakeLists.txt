#########################
#
# Add your unit tests here
#
#########################
set(TEST_SOURCE
  test_commandline.cc
  test_ibm.cc
  test_init.cc
  )
# Add test executable target
foreach(SOURCE_LINE ${TEST_SOURCE})
  string(REPLACE ".cc" "" FILE_LINE ${SOURCE_LINE})

  add_executable(${FILE_LINE} ${SOURCE_LINE} ${${PROJECT_NAME}_SOURCE})

  target_include_directories (
    ${FILE_LINE}
    PRIVATE 
    $<BUILD_INTERFACE:${${PROJECT_NAME}_SOURCE_DIR}/include>
    $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/>
    )

  target_link_libraries(
    ${FILE_LINE}
    gtest gtest_main gmock gmock_main
    ${${PROJECT_NAME}_LIBRARIES}
    )
  target_compile_features(${FILE_LINE}
    PRIVATE 
    cxx_std_17
    )

  add_test(${FILE_LINE} ${FILE_LINE})
endforeach()
add_test(test_ibm test_ibm ${CMAKE_SOURCE_DIR}/roms/IBM )
