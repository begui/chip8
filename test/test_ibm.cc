#include <iostream>
#include "chip8.hh"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

char *filename{nullptr};
TEST ( IBM, CheckMemory ) {
  Chip8 chip8;
  chip8.loadRom ( filename );

  auto i{Chip8::ProgramCounterStart};
  EXPECT_EQ ( 0x00, chip8.memory ( )[ i++ ] );
  EXPECT_EQ ( 0xE0, chip8.memory ( )[ i++ ] );
  EXPECT_EQ ( 0xA2, chip8.memory ( )[ i++ ] );
  EXPECT_EQ ( 0x2A, chip8.memory ( )[ i++ ] );
  EXPECT_EQ ( 0x60, chip8.memory ( )[ i++ ] );
  EXPECT_EQ ( 0x0C, chip8.memory ( )[ i++ ] );
  EXPECT_EQ ( 0x61, chip8.memory ( )[ i++ ] );
  EXPECT_EQ ( 0x08, chip8.memory ( )[ i++ ] );
  EXPECT_EQ ( 0xD0, chip8.memory ( )[ i++ ] );
  EXPECT_EQ ( 0x1F, chip8.memory ( )[ i++ ] );
  EXPECT_EQ ( 0x70, chip8.memory ( )[ i++ ] );
  EXPECT_EQ ( 0x09, chip8.memory ( )[ i++ ] );
  EXPECT_EQ ( 0xA2, chip8.memory ( )[ i++ ] );
  EXPECT_EQ ( 0x39, chip8.memory ( )[ i++ ] );
  EXPECT_EQ ( 0xD0, chip8.memory ( )[ i++ ] );
  EXPECT_EQ ( 0x1F, chip8.memory ( )[ i++ ] );
  // . . .
  EXPECT_EQ ( 0x00, chip8.memory ( )[ 640 ] );
  EXPECT_EQ ( 0xE0, chip8.memory ( )[ 641 ] );
  EXPECT_EQ ( 0x00, chip8.memory ( )[ 642 ] );
  EXPECT_EQ ( 0xE0, chip8.memory ( )[ 643 ] );

  EXPECT_EQ ( 0x00, chip8.memory ( )[ 644 ] );
  EXPECT_EQ ( 0x00, chip8.memory ( )[ 645 ] );

  SUCCEED ( );
}

TEST ( IBM, GameState ) {
  Chip8 chip8;
  chip8.loadRom ( filename );
  chip8.pause ( true );

  EXPECT_EQ ( 512, chip8.programCounter ( ) );
  EXPECT_EQ ( 0X00E0, chip8.opcodeCurrent ( ) );
  EXPECT_EQ ( 0X00E0, chip8.opcode ( ) );
  EXPECT_EQ ( 0X0000, chip8.indexRegister ( ) );
  EXPECT_EQ ( 0X0000, chip8.stackPointer ( ) );
  EXPECT_THAT ( chip8.display ( ), ::testing::Each (::testing::Eq ( 0 ) ) );
  EXPECT_THAT ( chip8.registers ( ), ::testing::Each (::testing::Eq ( 0 ) ) );
  chip8.stepCycle ( );
  EXPECT_EQ ( 514, chip8.programCounter ( ) );
  EXPECT_EQ ( 0XA22A, chip8.opcodeCurrent ( ) );
  EXPECT_EQ ( 0X00E0, chip8.opcode ( ) );
  EXPECT_EQ ( 0X0000, chip8.indexRegister ( ) );
  EXPECT_EQ ( 0X0000, chip8.stackPointer ( ) );
  EXPECT_THAT ( chip8.display ( ), ::testing::Each (::testing::Eq ( 0 ) ) );
  EXPECT_THAT ( chip8.registers ( ), ::testing::Each (::testing::Eq ( 0 ) ) );
  chip8.stepCycle ( );
  EXPECT_EQ ( 516, chip8.programCounter ( ) );
  EXPECT_EQ ( 0X600c, chip8.opcodeCurrent ( ) );
  EXPECT_EQ ( 0XA22A, chip8.opcode ( ) );
  EXPECT_EQ ( 0X22A, chip8.indexRegister ( ) );
  EXPECT_EQ ( 0X0000, chip8.stackPointer ( ) );
  EXPECT_THAT ( chip8.display ( ), ::testing::Each (::testing::Eq ( 0 ) ) );
  EXPECT_THAT ( chip8.registers ( ), ::testing::Each (::testing::Eq ( 0 ) ) );
  chip8.stepCycle ( );
  EXPECT_EQ ( 518, chip8.programCounter ( ) );
  EXPECT_EQ ( 0X6108, chip8.opcodeCurrent ( ) );
  EXPECT_EQ ( 0X600C, chip8.opcode ( ) );
  EXPECT_EQ ( 0X22A, chip8.indexRegister ( ) );
  EXPECT_EQ ( 0X0000, chip8.stackPointer ( ) );
  EXPECT_THAT ( chip8.display ( ), ::testing::Each (::testing::Eq ( 0 ) ) );
  EXPECT_THAT ( chip8.registers ( ), ::testing::ElementsAreArray ( {0x0C, 0x00, 0x00, 0x00, 0x00,  //
                                                                    0x00, 0x00, 0x00, 0x00, 0x00,  //
                                                                    0x00, 0x00, 0x00, 0x00, 0x00,  //
                                                                    0X00} ) );
  chip8.stepCycle ( );
  EXPECT_EQ ( 520, chip8.programCounter ( ) );
  EXPECT_EQ ( 0XD01F, chip8.opcodeCurrent ( ) );
  EXPECT_EQ ( 0X6108, chip8.opcode ( ) );
  EXPECT_EQ ( 0X22A, chip8.indexRegister ( ) );
  EXPECT_EQ ( 0X0000, chip8.stackPointer ( ) );
  EXPECT_THAT ( chip8.display ( ), ::testing::Each (::testing::Eq ( 0 ) ) );
  EXPECT_THAT ( chip8.registers ( ), ::testing::ElementsAreArray ( {0x0C, 0x08, 0x00, 0x00, 0x00,  //
                                                                    0x00, 0x00, 0x00, 0x00, 0x00,  //
                                                                    0x00, 0x00, 0x00, 0x00, 0x00,  //
                                                                    0X00} ) );

  chip8.stepCycle ( );
  EXPECT_EQ ( 522, chip8.programCounter ( ) );
  EXPECT_EQ ( 0X7009, chip8.opcodeCurrent ( ) );
  EXPECT_EQ ( 0XD01F, chip8.opcode ( ) );
  EXPECT_EQ ( 0X22A, chip8.indexRegister ( ) );
  EXPECT_EQ ( 0X0000, chip8.stackPointer ( ) );
  EXPECT_THAT ( chip8.display ( ), ::testing::Contains (::testing::Eq ( 1 ) ) );
  EXPECT_THAT ( chip8.registers ( ), ::testing::ElementsAreArray ( {0x0C, 0x08, 0x00, 0x00, 0x00,  //
                                                                    0x00, 0x00, 0x00, 0x00, 0x00,  //
                                                                    0x00, 0x00, 0x00, 0x00, 0x00,  //
                                                                    0X00} ) );
  chip8.stepCycle ( );
  EXPECT_EQ ( 524, chip8.programCounter ( ) );
  EXPECT_EQ ( 0XA239, chip8.opcodeCurrent ( ) );
  EXPECT_EQ ( 0X7009, chip8.opcode ( ) );
  EXPECT_EQ ( 0X22A, chip8.indexRegister ( ) );
  EXPECT_EQ ( 0X0000, chip8.stackPointer ( ) );
  EXPECT_THAT ( chip8.display ( ), ::testing::Contains (::testing::Eq ( 1 ) ) );
  EXPECT_THAT ( chip8.registers ( ), ::testing::ElementsAreArray ( {0x15, 0x08, 0x00, 0x00, 0x00,  //
                                                                    0x00, 0x00, 0x00, 0x00, 0x00,  //
                                                                    0x00, 0x00, 0x00, 0x00, 0x00,  //
                                                                    0X00} ) );

  chip8.stepCycle ( );
  EXPECT_EQ ( 526, chip8.programCounter ( ) );
  EXPECT_EQ ( 0XD01F, chip8.opcodeCurrent ( ) );
  EXPECT_EQ ( 0XA239, chip8.opcode( ) );
  EXPECT_EQ ( 0X239, chip8.indexRegister ( ) );
  EXPECT_EQ ( 0X0000, chip8.stackPointer ( ) );
  EXPECT_THAT ( chip8.display ( ), ::testing::Contains (::testing::Eq ( 1 ) ) );
  EXPECT_THAT ( chip8.registers ( ), ::testing::ElementsAreArray ( {0x15, 0x08, 0x00, 0x00, 0x00,  //
                                                                    0x00, 0x00, 0x00, 0x00, 0x00,  //
                                                                    0x00, 0x00, 0x00, 0x00, 0x00,  //
                                                                    0X00} ) );
  //...
}

int main ( int argc, char **argv ) {
  testing::InitGoogleTest ( &argc, argv );

  assert ( 2 == argc );
  filename = argv[ 1 ];

  return RUN_ALL_TESTS ( );
}
