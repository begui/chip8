#include "command_line.hh"
#include "gtest/gtest.h"

TEST ( Command_Line, Check_norom ) {
  CommandLine commandLine;
  commandLine.resetOptIndex();
  int argc = 1;
  char *argv[] = {
      "chip8",       //
  };
  auto result = commandLine.captureArg ( argc, argv );

  EXPECT_EQ ( false, result );
  EXPECT_EQ ( "", commandLine.rom ( ) );
  EXPECT_EQ ( false, commandLine.debug ( ) );
  EXPECT_EQ ( false, commandLine.fullscreen ( ) );
  SUCCEED ( );
}

TEST ( Command_Line, Check_rom ) {
  CommandLine commandLine;
  commandLine.resetOptIndex();
  int argc = 3;
  char *argv[] = {
      "chip8",       //
      "-r",         //
      "../roms/ROM"  //
  };
  auto result = commandLine.captureArg ( argc, argv );

  EXPECT_EQ ( true, result );
  EXPECT_EQ ( "../roms/ROM", commandLine.rom ( ) );
  EXPECT_EQ ( false, commandLine.debug ( ) );
  EXPECT_EQ ( false, commandLine.fullscreen ( ) );
  SUCCEED ( );
}

TEST ( Command_Line, Check_debug ) {
  CommandLine commandLine;
  commandLine.resetOptIndex();
  int argc = 4;
  char *argv[] = {
      "chip8",        //
      "-r",           //
      "../roms/ROM",  //
      "-d"            //
  };
  auto result = commandLine.captureArg ( argc, argv );

  EXPECT_EQ ( true, result );
  EXPECT_EQ ( "../roms/ROM", commandLine.rom ( ) );
  EXPECT_EQ ( true, commandLine.debug ( ) );
  EXPECT_EQ ( false, commandLine.fullscreen ( ) );
  SUCCEED ( );
}

TEST ( Command_Line, Check_fullscreen ) {
  CommandLine commandLine;
  commandLine.resetOptIndex();
  int argc = 4;
  char *argv[] = {
      "chip8",        //
      "-r",           //
      "../roms/ROM",  //
      "-f"            //
  };
  auto result = commandLine.captureArg ( argc, argv );

  EXPECT_EQ ( true, result );
  EXPECT_EQ ( "../roms/ROM", commandLine.rom ( ) );
  EXPECT_EQ ( false, commandLine.debug ( ) );
  EXPECT_EQ ( true, commandLine.fullscreen ( ) );
  SUCCEED ( );
}

TEST ( Command_Line, Check_all ) {
  CommandLine commandLine;
  commandLine.resetOptIndex();
  int argc = 5;
  char *argv[] = {
      "chip8",        //
      "-r",           //
      "../roms/ROM",  //
      "-d",           //
      "-f"            //
  };
  auto result = commandLine.captureArg ( argc, argv );

  EXPECT_EQ ( true, result );
  EXPECT_EQ ( "../roms/ROM", commandLine.rom ( ) );
  EXPECT_EQ ( true, commandLine.debug ( ) );
  EXPECT_EQ ( true, commandLine.fullscreen ( ) );
  SUCCEED ( );
}

int main ( int argc, char **argv ) {
  testing::InitGoogleTest ( &argc, argv );
  return RUN_ALL_TESTS ( );
}
