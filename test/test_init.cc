#include "chip8.hh"
#include "gtest/gtest.h"

TEST ( Init, Default_Values ) {
  auto val = Chip8::WidthSize;
  EXPECT_EQ ( 64, val );

  val = Chip8::HeightSize;
  EXPECT_EQ ( 32, val );

  val = Chip8::MemorySize;
  EXPECT_EQ ( 4096, val );

  val = Chip8::ProgramCounterStart;
  EXPECT_EQ ( 512, val );

  val = Chip8::ProgramSize;
  EXPECT_EQ ( 4096 - 512, val );

  val = Chip8::DisplaySize;
  EXPECT_EQ ( 64 * 32, val );

  val = Chip8::StackSize;
  EXPECT_EQ ( 16, val );

  val = Chip8::RegisterSize;
  EXPECT_EQ ( 16, val );

  val = Chip8::KeySize;
  EXPECT_EQ ( 16, val );

  SUCCEED ( );
}

TEST ( Init, Default_Size ) {
  Chip8 chip8;
  chip8.reset ( );

  auto sizeMax = Chip8::MemorySize;
  EXPECT_EQ ( sizeMax, chip8.memory( ).size ( ) );
  sizeMax = Chip8::RegisterSize;
  EXPECT_EQ ( sizeMax, chip8.registers ( ).size ( ) );
  sizeMax = Chip8::DisplaySize;
  EXPECT_EQ ( sizeMax, chip8.display ( ).size ( ) );
  sizeMax = Chip8::StackSize;
  EXPECT_EQ ( sizeMax, chip8.stack( ).size ( ) );

  SUCCEED ( );
}

TEST ( Font, CheckFontMemory ) {
  Chip8 chip8;
  chip8.reset ( );

  auto mem = chip8.memory ( );

  // 0
  EXPECT_EQ ( 0xF0, mem[ 0 ] );
  EXPECT_EQ ( 0xF0, mem[ 4 ] );
  // 1
  EXPECT_EQ ( 0x20, mem[ 5 ] );
  EXPECT_EQ ( 0x70, mem[ 9 ] );
  // 2
  EXPECT_EQ ( 0xF0, mem[ 10 ] );
  EXPECT_EQ ( 0xF0, mem[ 14 ] );
  // 3
  EXPECT_EQ ( 0xF0, mem[ 15 ] );
  EXPECT_EQ ( 0xF0, mem[ 19 ] );
  // 4
  EXPECT_EQ ( 0x90, mem[ 20 ] );
  EXPECT_EQ ( 0x10, mem[ 24 ] );
  // 5
  EXPECT_EQ ( 0xF0, mem[ 25 ] );
  EXPECT_EQ ( 0xF0, mem[ 29 ] );
  // 6
  EXPECT_EQ ( 0xF0, mem[ 30 ] );
  EXPECT_EQ ( 0xF0, mem[ 34 ] );
  // 7
  EXPECT_EQ ( 0xF0, mem[ 35 ] );
  EXPECT_EQ ( 0x40, mem[ 39 ] );
  // 8
  EXPECT_EQ ( 0xF0, mem[ 40 ] );
  EXPECT_EQ ( 0xF0, mem[ 44 ] );
  // 9
  EXPECT_EQ ( 0xF0, mem[ 45 ] );
  EXPECT_EQ ( 0xF0, mem[ 49 ] );
  // A
  EXPECT_EQ ( 0xF0, mem[ 50 ] );
  EXPECT_EQ ( 0x90, mem[ 54 ] );
  // B
  EXPECT_EQ ( 0xE0, mem[ 55 ] );
  EXPECT_EQ ( 0xE0, mem[ 59 ] );
  // C
  EXPECT_EQ ( 0xF0, mem[ 60 ] );
  EXPECT_EQ ( 0xF0, mem[ 64 ] );
  // D
  EXPECT_EQ ( 0xE0, mem[ 65 ] );
  EXPECT_EQ ( 0xE0, mem[ 69 ] );
  // E
  EXPECT_EQ ( 0xF0, mem[ 70 ] );
  EXPECT_EQ ( 0xF0, mem[ 74 ] );
  // F
  EXPECT_EQ ( 0xF0, mem[ 75 ] );
  EXPECT_EQ ( 0x80, mem[ 79 ] );

  EXPECT_EQ ( 0x00, mem[ 80 ] );

  SUCCEED ( );
}

int main ( int argc, char **argv ) {
  testing::InitGoogleTest ( &argc, argv );
  return RUN_ALL_TESTS ( );
}
