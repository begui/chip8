cmake_minimum_required(VERSION 3.9.1)
project(chip8 VERSION 2020.8.0 DESCRIPTION "Chip8" LANGUAGES CXX)
#########################
#
# The version number.
#
#########################
set(CHIP8_VERSION_STRING "${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH}")
# configure a header file to pass some of the cmake settings to the source code
configure_file(
  ${CMAKE_CURRENT_SOURCE_DIR}/include/version.hh.in 
  ${PROJECT_BINARY_DIR}/version.hh
) 

set( CMAKE_EXPORT_COMPILE_COMMANDS ON ) #For Vim 

#########################
#
# Find packages
#
#########################
find_package(PkgConfig REQUIRED)
foreach(dependencies zlib sdl2 SDL2_ttf )
  pkg_search_module(${dependencies} REQUIRED ${dependencies})
  list(APPEND ${PROJECT_NAME}_LIBRARIES ${${dependencies}_LIBRARIES})
endforeach()
#########################
#
# Set Sources 
#
#########################
set( ${PROJECT_NAME}_SOURCE
  ${CMAKE_SOURCE_DIR}/src/chip8.cc
  ${CMAKE_SOURCE_DIR}/src/command_line.cc
  )
#########################
#
# Build Binary 
#
#########################
add_executable(${PROJECT_NAME} src/main.cc ${${PROJECT_NAME}_SOURCE})
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/external/gd-sdl2 EXCLUDE_FROM_ALL)

target_include_directories ( ${PROJECT_NAME}
  PRIVATE 
  $<BUILD_INTERFACE:${${PROJECT_NAME}_SOURCE_DIR}/include>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/>
  )

target_link_libraries( ${PROJECT_NAME}
  PRIVATE 
  ${${PROJECT_NAME}_LIBRARIES}
  GD_SDL2
  )

target_compile_options(${PROJECT_NAME}
  PRIVATE 
  $<$<CXX_COMPILER_ID:GNU>:-Wall>
  $<$<CXX_COMPILER_ID:GNU>:-Werror>
  $<$<CXX_COMPILER_ID:GNU>:-Wshadow>
  $<$<CXX_COMPILER_ID:GNU>:-Wextra>
  #$<$<CXX_COMPILER_ID:GNU>:-Wconversion>
  )

target_compile_features(${PROJECT_NAME}
  PRIVATE 
  cxx_std_17
  )

target_compile_definitions(
  ${PROJECT_NAME}
  PRIVATE 
  USE_SDL2_TTF=1
  USE_SDL2_IMAGE=0
  USE_SDL2_MIXER=0
  )
##########################################################
#
# Build Test
#
##########################################################
if(BUILD_TEST) 
  message(STATUS "BUILDING TESTS")
  add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/external/googletest EXCLUDE_FROM_ALL)
  enable_testing()
  add_subdirectory(test)
endif()
